# frozen_string_literal: true

# messages controller
class MessagesController < ApplicationController
  before_action :authenticate_user!, except: %i[index show]
  before_action :set_message, only: %i[show edit update destroy]

  def index
    @messages = Message.all.order(created_at: :desc)
  end

  def show; end

  def new
    @message = current_user.messages.build
  end

  def edit; end

  def create
    @message = current_user.messages.build(message_params)
    
    if @message.save
      redirect_to root_path
    else
      render :new
    end
  end

  def update
    if @message.update(message_params)
      redirect_to message_path
    else
      render :edit
    end
  end

  def destroy
    @message.destroy
    redirect_to root_path
  end

  private

  def set_message
    @message = Message.find(params[:id])
  end

  def message_params
    params.require(:message).permit(:title, :description)
  end
end
