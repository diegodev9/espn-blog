# frozen_string_literal: true

class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_message
  before_action :set_comment, except: %i[create]
  before_action :check_owner, only: %i[edit update destroy]

  def create
    @comment = @message.comments.create(comment_params)
    @comment.user_id = current_user.id

    if @comment.save
      redirect_to message_path(@message)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @comment.update(comment_params)
      redirect_to message_path(@message)
    else
      render :edit
    end
  end

  def destroy
    @comment.destroy
    redirect_to message_path(@message)
  end

  private

  def set_message
    @message = Message.find(params[:message_id])
  end

  def set_comment
    @comment = Comment.find(params[:id])
  end

  def check_owner
    redirect_to message_path(@message), notice: 'No puedes editar un comentario que no es tuyo' unless current_user.id == @comment.user_id
  end

  def comment_params
    params.require(:comment).permit(:content)
  end
end
